import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.element;
import static org.openqa.selenium.By.cssSelector;

public class SignUpPage {

    public final static String relativeOrAbsoluteUrl = "/us/signup/";

    private By emailField = cssSelector("#register-email");
    private By confirmEmailField = cssSelector("#register-confirm-email");
    private By passwordField = cssSelector("#register-password");
    private By displayNameField = cssSelector("#register-displayname");
    private By monthDropDown = cssSelector("#register-dob-month");
    private By dayField = cssSelector("#register-dob-day");
    private By yearField = cssSelector("#register-dob-year");
    private By shareCheckbox = cssSelector("#register-thirdparty");
    private By registerButton = cssSelector("#register-button-email-submit");

    public SignUpPage open() {
        Selenide.open(relativeOrAbsoluteUrl);

        return this;
    }


    public SignUpPage typeEmail(String email) {
        $(emailField).shouldHave(Condition.appears).sendKeys(email);
        return this;
    }

    public SignUpPage typeConfirmEmailField(String email) {
        $(confirmEmailField).shouldBe(Condition.empty).setValue(email);
        return this;
    }

    public SignUpPage typePassword(String password) {
        $(passwordField).shouldNotHave(Condition.text("Password")).val(password);
        return this;
    }

    //Метод выбора месяца
    public SignUpPage setMonth(String month) {
        $(monthDropDown).shouldNotBe(Condition.disabled).selectOption(month);
        return this;
    }

    //Метод для заполнения поля Day
    public SignUpPage typeDay(String day) {
        $(dayField).sendKeys(day);
        return this;
    }

    //Метод для заполнения поля Year
    public SignUpPage typeYear(String year) {
        $(yearField).sendKeys(year);
        return this;
    }
}
